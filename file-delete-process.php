<?php 
include 'database_conn.php';
header('Content-Type: application/json; charset=utf-8');
//$("div#filelist ul > li:nth-of-type(1)").data('id');
$success = false;
 $message = "";

if(isset($_POST['id'])){
	$id = $_POST['id'];
	 $sql = "DELETE FROM file_data WHERE ID=$id";
	 
	if ($conn->query($sql) === TRUE) {
	  $message =  "Record deleted successfully";
	  $success = true;
	  
	  $path = $_POST['path'];
	  unlink(getcwd().parse_url($path, PHP_URL_PATH));
	} else {
	  $message = "Error deleting record: " . $conn->error;
	   $success = false;
	} 
}
  echo json_encode(array('result' => $message , 'succes' => $success));
include 'close_conn.php';
?>