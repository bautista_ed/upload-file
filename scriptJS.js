
reload_list();
function reload_list(){
	var ajax_url = "http://upload-file.test/loading-file-list.php";
		 jQuery.ajax({
			url:ajax_url,
			type: 'get',
			contentType: "application/json",
        dataType: "json",
			beforeSend: function() {
			
			},
			complete: function() {
				
			},
			success : function( response ){
				console.log(response);
				if(response['result'] != ""){
					jQuery("div#filelist").html(response['result'])
				}
			}
		});  
}


jQuery("document").ready(function(){
	 
	 jQuery('body').on('click' , ".delete-file" ,function(){
			var target = jQuery(this).parents("li");
			var id = target.data("id");
			var path = target.data("path");
			console.log(id);
			
			let text;
			if (confirm("You want to delete?") == true) {
			  text = "You pressed OK!";
			  delete_record(id , path);
			target.fadeOut('slow', function(){ jQuery(this).remove(); });
			} else {
			  text = "You canceled!";
			}
			
						
		})
})
function delete_record(id = 0 , path = ''){
	
	if(id == 0)return;
	var ajax_url = "http://upload-file.test/file-delete-process.php";
		 jQuery.ajax({
			url:ajax_url,
			type: 'post',
			data : {"id" : id , "path" : path},
			dataType: "json",
			beforeSend: function() {
			
			},
			complete: function() {
				
			},
			success : function( response ){
				console.log(response);
				/* if(response['result'] != ""){
					jQuery("div#filelist").html(response['result'])
				} */
			}
		});  
}

jQuery(document).ready(function(){
	// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");


// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}


// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
   if (jQuery(event.target).attr('id') == 'editFile') {
		jQuery('#editFile').fadeOut(function(){jQuery(this).hide()});
	  } 
 
}
	
})

// File Upload
// 
function ekUpload(){
  function Init() {

    console.log("Upload Initialised");

    var fileSelect    = document.getElementById('file-upload'),
        fileDrag      = document.getElementById('file-drag'),
        submitButton  = document.getElementById('submit-button');

    fileSelect.addEventListener('change', fileSelectHandler, false);

    // Is XHR2 available?
    var xhr = new XMLHttpRequest();
    if (xhr.upload) {
      // File Drop
      fileDrag.addEventListener('dragover', fileDragHover, false);
      fileDrag.addEventListener('dragleave', fileDragHover, false);
      fileDrag.addEventListener('drop', fileSelectHandler, false);
    }
  }

  function fileDragHover(e) {
    var fileDrag = document.getElementById('file-drag');

    e.stopPropagation();
    e.preventDefault();

    fileDrag.className = (e.type === 'dragover' ? 'hover' : 'modal-body file-upload');
  }

  function fileSelectHandler(e) {
    // Fetch FileList object
    var files = e.target.files || e.dataTransfer.files;

    // Cancel event and hover styling
    fileDragHover(e);

    // Process all File objects
    for (var i = 0, f; f = files[i]; i++) {
      parseFile(f);
     
    }
  }

  // Output
  function output(msg) {
    // Response
    var m = document.getElementById('messages');
    m.innerHTML = msg;
  }

  function parseFile(file) {

    console.log(file.name);
    output(
      '<strong>' + encodeURI(file.name) + '</strong>'
    );
    
    // var fileType = file.type;
    // console.log(fileType);
    var imageName = file.name;

    var isGood = (/\.(?=gif|jpg|png|jpeg)/gi).test(imageName);
    if (isGood) {
      document.getElementById('start').classList.add("hidden");
      document.getElementById('response').classList.remove("hidden");
      document.getElementById('notimage').classList.add("hidden");
      // Thumbnail Preview
      document.getElementById('file-image').classList.remove("hidden");
      document.getElementById('file-image').src = URL.createObjectURL(file);
    }
    else {
      document.getElementById('file-image').classList.add("hidden");
      document.getElementById('notimage').classList.remove("hidden");
      document.getElementById('start').classList.remove("hidden");
      document.getElementById('response').classList.add("hidden");
      document.getElementById("file-upload-form").reset();
    }
  }

  function setProgressMaxValue(e) {
    var pBar = document.getElementById('file-progress');

    if (e.lengthComputable) {
      pBar.max = e.total;
    }
  }

  function updateFileProgress(e) {
    var pBar = document.getElementById('file-progress');

    if (e.lengthComputable) {
      pBar.value = e.loaded;
    }
  }

  // Check for the various File API support.
  if (window.File && window.FileList && window.FileReader) {
    Init();
  } else {
    document.getElementById('file-drag').style.display = 'none';
  }
}
ekUpload();



// File Upload
// 
function ekUpload_edit(){
  function Init() {

    console.log("Upload Initialised");

    var fileSelect    = document.getElementById('file-upload-edit'),
        fileDrag      = document.getElementById('file-drag-edit'),
        submitButton  = document.getElementById('submit-button-edit');

    fileSelect.addEventListener('change', fileSelectHandler, false);

    // Is XHR2 available?
    var xhr = new XMLHttpRequest();
    if (xhr.upload) {
      // File Drop
      fileDrag.addEventListener('dragover', fileDragHover, false);
      fileDrag.addEventListener('dragleave', fileDragHover, false);
      fileDrag.addEventListener('drop', fileSelectHandler, false);
    }
  }

  function fileDragHover(e) {
    var fileDrag = document.getElementById('file-drag-edit');

    e.stopPropagation();
    e.preventDefault();

    fileDrag.className = (e.type === 'dragover' ? 'hover' : 'modal-body file-upload');
  }

  function fileSelectHandler(e) {
    // Fetch FileList object
    var files = e.target.files || e.dataTransfer.files;

    // Cancel event and hover styling
    fileDragHover(e);

    // Process all File objects
    for (var i = 0, f; f = files[i]; i++) {
      parseFile(f);
     
    }
  }

  // Output
  function output(msg) {
    // Response
    var m = document.getElementById('messages-edit');
    m.innerHTML = msg;
  }

  function parseFile(file) {

    console.log(file.name);
    output(
      '<strong>' + encodeURI(file.name) + '</strong>'
    );
    
    // var fileType = file.type;
    // console.log(fileType);
    var imageName = file.name;

    var isGood = (/\.(?=gif|jpg|png|jpeg)/gi).test(imageName);
    if (isGood) {
      document.getElementById('start-edit').classList.add("hidden");
      document.getElementById('response-edit').classList.remove("hidden");
      document.getElementById('notimage-edit').classList.add("hidden");
      // Thumbnail Preview
      document.getElementById('file-image-edit').classList.remove("hidden");
      document.getElementById('file-image-edit').src = URL.createObjectURL(file);
    }
    else {
      document.getElementById('file-image-edit').classList.add("hidden");
      document.getElementById('notimage-edit').classList.remove("hidden");
      document.getElementById('start-edit').classList.remove("hidden");
      document.getElementById('response-edit').classList.add("hidden");
      document.getElementById("file-upload-form-edit").reset();
    }
  }

  function setProgressMaxValue(e) {
    var pBar = document.getElementById('file-progress-edit');

    if (e.lengthComputable) {
      pBar.max = e.total;
    }
  }

  function updateFileProgress(e) {
    var pBar = document.getElementById('file-progress-edit');

    if (e.lengthComputable) {
      pBar.value = e.loaded;
    }
  }

  // Check for the various File API support.
  if (window.File && window.FileList && window.FileReader) {
    Init();
  } else {
    document.getElementById('file-drag-edit').style.display = 'none';
  }
}
ekUpload_edit();

 jQuery("document").ready(function(){
	 
	 
	 
	 
	jQuery('div#myModal input[type="submit"]').on('click' , function(event ){
		 event.preventDefault();
		 
		 var file_data = jQuery('#file-upload').prop('files')[0];  
		var uploadfile = new FormData();
		
		uploadfile.append('file_upload', file_data);
		uploadfile.append('Title', jQuery('#Title').val());
		
		if(jQuery('#Title').val() == "" || file_data == null){
			alert("Fields Required.");
			return;
		}
		var ajax_url = "http://upload-file.test/upload-process.php";
		 jQuery.ajax({
			url:ajax_url,
			type: 'post',
			processData: false,
			contentType: false,
        dataType: "json",
			 		/* 	enctype: 'multipart/form-data', */
		
			data:  uploadfile,
			beforeSend: function() {
			
			},
			complete: function() {
				
			},
			success : function( response ){
				console.log(response);
				 if(response["html"] != "")jQuery( "div#filelist ul" ).prepend( response["html"] );
				 
				 var status_card = ' <img id="editfilebtn" src="/resources/img/edit_card.png" style="    float: right;    margin: 0;    background: #eaeffb;    border-radius: 50%;    border: 1px dashed #83a2e1;" />';
				 
				 jQuery('div#filelist ul > li:nth-of-type(1) .status').delay(3000).fadeOut('slow', function(){ jQuery(this).find("img").remove(); jQuery(this).html(status_card).fadeIn()});
				 // $("#div3").delay(1000).fadeIn();   
				
			},
			 xhr: function() {
			var xhr = new window.XMLHttpRequest();
			
			xhr.upload.addEventListener('loadstart', function(evt){
				var pBar = document.getElementById('file-progress');

				if (evt.lengthComputable) {
				  pBar.max = evt.total;
				}
			}, false);
			xhr.upload.addEventListener("progress", function(evt) {
			  if (evt.lengthComputable) {
				var percentComplete = evt.loaded / evt.total;
				percentComplete = parseInt(percentComplete * 100);
				console.log(percentComplete);
				jQuery('progress#file-progress span' ).html(percentComplete);
				 var pBar = document.getElementById('file-progress');

					if (evt.lengthComputable) {
					  pBar.value = evt.loaded;
					}
				
		/* var $link = $('.'+ids);
				  var $img = $link.find('i'); 
				  $link.html('Uploading..('+percentComplete+'%)');
				  $link.append($img); */
			  }
			}, false);

			xhr.onreadystatechange = function(e) {
				console.log(xhr);
				var modal = document.getElementById("myModal");	
			  if (xhr.readyState == 4) {
				 var json = JSON.parse(xhr.response);
				var pBar = document.getElementById('file-progress');
				alert(json['message']);
				jQuery('img#file-image').attr('class' , "hidden");
				jQuery('div#start').removeAttr('class');
				jQuery('.uploader #response #messages strong').html('');
				 pBar.style.display = 'none';
				
				jQuery(':input','#file-upload-form').not(':button, :submit, :reset, :hidden').val('').prop('checked', false).prop('selected', false);
				jQuery('#file-upload').val(null);
				
				if(json['success'] == true){
					modal.style.display = "none";
				}
			  }
			};


			return xhr;
		  },
		});  
	}); 
	
	
	
	jQuery('div#editFile input[type="submit"]').on('click' , function(event ){
		 event.preventDefault();
		 var data = jQuery( "div#editFile" ).data( "data");
		 var file_data = jQuery('#file-upload-edit').prop('files')[0];  
		var uploadfile = new FormData();
		
		uploadfile.append('file_upload', file_data);
		uploadfile.append('Titleedit', jQuery('#Title-edit').val());
		uploadfile.append('action','edit');
		uploadfile.append('id',data['id']);
		
		if(jQuery('#Title-edit').val() == "" ){
			alert("Fields Required.");
			return;
		}
		var ajax_url = "http://upload-file.test/upload-process.php";
		 jQuery.ajax({
			url:ajax_url,
			type: 'post',
			processData: false,
			contentType: false,
        dataType: "json",
			 		/* 	enctype: 'multipart/form-data', */
		
			data:  uploadfile,
			beforeSend: function() {
			
			},
			complete: function() {
				
			},
			success : function( response ){
				console.log(response);
				
				
				var element = jQuery( "div#editFile" ).data('element');
				
				if(response['data']['path'] !== undefined)jQuery(element).attr('data-path' , response['data']['path']);
				
				 if(response["html"] != "")jQuery(element).html(response['html']);
				 
				var status_card = ' <img id="editfilebtn" src="/resources/img/edit_card.png" style="    float: right;    margin: 0;    background: #eaeffb;    border-radius: 50%;    border: 1px dashed #83a2e1;" />';
				 
				element.find('.status').delay(3000).fadeOut('slow', function(){ jQuery(this).find("img").remove(); jQuery(this).html(status_card).fadeIn()});
				  
				if(response["data"] != null)element.find("h3").html(response["data"]['title']);
			},
			 xhr: function() {
			var xhr = new window.XMLHttpRequest();
			
			xhr.upload.addEventListener('loadstart', function(evt){
				var pBar = document.getElementById('file-progress-edit');

				if (evt.lengthComputable) {
				  pBar.max = evt.total;
				}
			}, false);
			xhr.upload.addEventListener("progress", function(evt) {
			  if (evt.lengthComputable) {
				var percentComplete = evt.loaded / evt.total;
				percentComplete = parseInt(percentComplete * 100);
				console.log(percentComplete);
				jQuery('progress#file-progress-edit span' ).html(percentComplete);
				 var pBar = document.getElementById('file-progress-edit');

					if (evt.lengthComputable) {
					  pBar.value = evt.loaded;
					}
				
		/* var $link = $('.'+ids);
				  var $img = $link.find('i'); 
				  $link.html('Uploading..('+percentComplete+'%)');
				  $link.append($img); */
			  }
			}, false);

			xhr.onreadystatechange = function(e) {
				console.log(xhr);
				var modal = document.getElementById("editFile");	
			  if (xhr.readyState == 4) {
				 var json = JSON.parse(xhr.response);
				var pBar = document.getElementById('file-progress-edit');
				alert(json['message']);
				jQuery('img#file-image-edit').attr('class' , "hidden");
				jQuery('div#start-edit').removeAttr('class');
				jQuery('.uploader #response-edit #messages-edit strong').html('');
				 pBar.style.display = 'none';
				
				jQuery(':input','#file-upload-form-edit').not(':button, :submit, :reset, :hidden').val('').prop('checked', false).prop('selected', false);
				jQuery('#file-upload').val(null);
				
				if(json['success'] == true){
					modal.style.display = "none";
				}
			  }
			};


			return xhr;
		  },
		});  
	}); 
	
	
}); 


jQuery(document).ready(function(){
	
	
	jQuery( "body" ).on("dblclick" , 'div#filelist ul li' ,function() {
	  jQuery(this).find( "#editfilebtn" ).click();
	});
	
	var editfile = document.getElementById("editFile");
	var editbtn = document.getElementById("editfilebtn");

	
	jQuery("body").on("click" , '#editfilebtn' , function(){
		jQuery('#editFile').show();
		var target = jQuery(this).parents("li");
		var id = target.attr('data-id');
		var path = target.attr('data-path');
		var title = target.find("h3").text();
		
		jQuery( "div#editFile" ).data( "data", { 'id': id , 'path' : path , 'title' : title} );
		jQuery( "div#editFile" ).data('element' , target);
		console.log(path);
		jQuery('#editFile input#Title-edit').val(title);
		
		 
		var isGood = (/\.(?=gif|jpg|png|jpeg)/gi).test(path);
		if (isGood) {
		  document.getElementById('start-edit').classList.add("hidden");
		  document.getElementById('response-edit').classList.remove("hidden");
		  document.getElementById('notimage-edit').classList.add("hidden");
		  // Thumbnail Preview
		  document.getElementById('file-image-edit').classList.remove("hidden");
		jQuery('#file-image-edit').attr('src' , path);
		}
		else {
		  document.getElementById('file-image-edit').classList.add("hidden");
		  document.getElementById('notimage-edit').classList.remove("hidden");
		  document.getElementById('start-edit').classList.remove("hidden");
		  document.getElementById('response-edit').classList.add("hidden");
		  document.getElementById("file-upload-form-edit").reset();
		  jQuery('#file-image-edit').attr('src' , path);
		}
		
		
	})
	jQuery("body").on("click" , '.close' , function(){
		jQuery('#editFile').fadeOut(function(){jQuery(this).hide()});
	})
	

})