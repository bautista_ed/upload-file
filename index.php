<?php 
include 'database_conn.php';

?>
<html>
<head>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<!-- Trigger/Open The Modal -->
<link rel="stylesheet" href="styles.css">
</head>
<body>
<!-- HTML !-->
<button id="myBtn" class="button-65" role="button">Upload File</button>

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <h2>File Upload & Image Preview</h2>
<div class="content-modal">
<!-- Upload  -->
<form id="file-upload-form" class="uploader" >
  <input required id="file-upload" type="file" name="fileUpload" accept="image/*" />

  <label for="file-upload" id="file-drag">
    <img id="file-image" src="#" alt="Preview" class="hidden">
    <div id="start">
      <i class="fa fa-download" aria-hidden="true"></i>
      <div>Select a file or drag here</div>
      <div id="notimage" class="hidden">Please select an image</div>
      <span id="file-upload-btn" class="btn btn-primary">Select a file</span>
    </div>
    <div id="response" class="hidden">
      <div id="messages"></div>
      <progress class="progress" id="file-progress" value="0">
        <span>0</span>%
      </progress>
    </div>
  </label>
  
	   <label for="Title" class="field-label">Title</label>
	  <input required type="text" id="Title" name="Title" class="field-input"><br><br>
	 
	  <input type="submit" value="Submit">
</form>
</div>
  </div>

</div>

<div id="filelist">
  
</div>



<!-- The Modal -->
<div id="editFile" class="modaledit">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <h2>Edit File</h2>
<div class="content-modal">
<!-- Upload  -->
<form id="file-upload-form" class="uploader-edit" >
  <input required id="file-upload-edit" type="file" name="fileUpload" accept="image/*" />

  <label for="file-upload-edit" id="file-drag-edit">
    <img id="file-image-edit" src="#" alt="Preview" class="hidden">
    <div id="start-edit">
      <i class="fa fa-download" aria-hidden="true"></i>
      <div>Select a file or drag here</div>
      <div id="notimage-edit" class="hidden">Please select an image</div>
      <span id="file-upload-btn-edit" class="btn btn-primary">Select a file</span>
    </div>
    <div id="response-edit" class="hidden">
      <div id="messages-edit"></div>
      <progress class="progress" id="file-progress-edit" value="0">
        <span>0</span>%
      </progress>
    </div>
  </label>
   <label for="Title" class="field-label">Title</label>
	  <input required type="text" id="Title-edit" name="Title" class="field-input"><br><br>
	 
	  
	  <input type="submit" value="Update">
</form>
</div>
  </div>
</body>
<script src="scriptJS.js" ></script>
</html>
<?php 
include 'close_conn.php';

?>