<?php 

$servername = "localhost";
$username = "root";
$password = "";

$ServerURL = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST'];

// Create connection
$conn = new mysqli($servername, $username, $password);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
// echo "Connected successfully";


$sql = "CREATE DATABASE IF NOT EXISTS upload_file_data";
if ($conn->query($sql) === TRUE) {
  // echo "Database created successfully";
} else {
  echo "Error creating database: " . $conn->error;
}

mysqli_select_db($conn, 'upload_file_data');

$sql = "CREATE TABLE IF NOT EXISTS file_data (
ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
title VARCHAR(30),
thumbnail VARCHAR(500) ,
filename VARCHAR(50)	,
date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=INNODB;";

if ($conn->query($sql) === TRUE) {
  // echo "Table MyGuests created successfully";
} else {
  echo "Error creating table: " . $conn->error;
}


//
?>