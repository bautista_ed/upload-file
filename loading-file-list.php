<?php 
include 'database_conn.php';
header('Content-Type: application/json; charset=utf-8');
$sql = "SELECT * FROM file_data ORDER BY ID DESC;";		
if ($result = $conn->query($sql)) {
	ob_start();
	?>
	<ul>
	<?php
  while ($row = (array)$result->fetch_object()) :
  /* echo "<pre>";
  print_r($row ); */
    ?>
	<li data-id='<?=$row['ID']?>' data-path="<?=$row['thumbnail']?>">
		<div class="image-content-card" style="
    display: inline-flex;
    width: 100%;    padding: 25px 25px;
">
      <img src="\resources\img\filenew.png" />
	  <div style="
    float: left;
    width: 70%;
	    text-align: left;
">
      <h3><?=$row['title']?></h3>
      <p>Path: <?=$row['thumbnail']?></p>
	  </div>
	  <div style="
    width: 13%;
">
<div class="status">

	   <img id="editfilebtn" src="\resources\img\edit_card.png"  />
</div>
	   </div>
	   </div>
	  <div class="footer-detail" style="
    display: flex;
    width: 100%;padding: 12px;background: #eaeffb;
">
<div style="
    width: 75%;
    display: flex;
">
	  <p style="  font-size: 12px!important;text-align: left;padding-left: 21px;"><span style="font-family: sans-serif;font-weight: 700;font-size: 14px!important;">ID</span> : <?=$row['ID']?></p>
	  <p style="  font-size: 12px!important;text-align: left;padding-left: 21px;"><span style="font-family: sans-serif;font-weight: 700;font-size: 14px!important;">Filename</span> : <?=$row['filename']?></p>
	  <p style="  font-size: 12px!important;text-align: left;padding-left: 21px;"><span style="font-family: sans-serif;font-weight: 700;font-size: 14px!important;">Date</span> : <?=$row['date']?></p>
		<!-- HTML !-->
		 </div>
	 <div style="
   width: 22%;
">
	<button style="float: right;width: 46%;font-size: 10px;padding: 0px;margin: 0px;line-height: 20.4929px;font-family: sans-serif;font-weight: 100;border-radius: 3px;" class="button-44 delete-file" role="button">Delete</button>
	</div>
	 </div>
    </li>
	<?php
  endwhile;
  
  ?>
  </ul>
  <?php
  $result->free_result();
  
  $output = ob_get_clean();

  // echo $output; 
  
  echo json_encode(array('result' => $output));
}

?>

<?php

include 'close_conn.php';

?>